#include <stdlib.h>
#include <stdio.h>


int temp(int degreeF) {
	int degreeC =  (degreeF-32) * 5/9;
	return degreeC;
}

int main() {
	int degreeF;
	printf("Fahrenheit-Celsius \n");
	for(degreeF = 0; degreeF <= 300; degreeF += 20) {
		int degreeC = (int) temp(degreeF);
		printf("%d	%d \n", degreeF, degreeC);
		
	}	

}



