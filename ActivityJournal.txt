SWEN-250 Activity Reflection

Name: Daniel Mironiuk

================================================================
Start Part 1
Estimated Time	00 15 min
Plan: To have a helper function for solving the value of the 
degreeC
(Order of implementation, testing approach, etc.)
- My order of implementation was starting with the helper function
which solves the function to solve the value of Celsius and then work on 
my main function which prints out the values

Complete
Actual Time   00 10 min
Observations:The method of my functionality was suburb and starting with
the helper function was more than ideal to keep the code in the main 
function concise
(Lessons learned, problems encountered, obstacles overcome, etc.)
To continue with my methodology and logic/ reasoning standards that i've 
been continuing with. No problems of grand nature, just some syntax errors.

================================================================
Start Part 2
Estimated Time	00 02 
Plan: I thought I was going to cast everything as a double and everything
would be point easy.
(Order of implementation, testing approach, etc.)
remove the int's and replace them for doubles

Complete
Actual Time	00 05
Observations: It took longer because I had to cast everything in as
double in the temp function where the equation relies.
(Lessons learned, problems encountered, obstacles overcome, etc.)
My lesson is to be careful when casting because you may recieve and
unintentional output

================================================================
