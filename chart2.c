 #include <stdlib.h>
 #include <stdio.h>
 
 
double temp(int degreeF) {
         double degreeC = (double) (degreeF-32) * 5/9;
         return degreeC;
 }
 
 int main() {
         int degreeF;
         printf("Fahrenheit-Celsius \n");
         for(degreeF = 0; degreeF <= 300; degreeF += 20) {
                 double degreeC = temp(degreeF);
                 printf("%d	      %.1f \n", degreeF, degreeC);
 
         }
 
 }

